from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'NRank.views.home', name='home'),
    # url(r'^NRank/', include('NRank.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),

    # Default page
    (r'^$', 'oursite.views.home.index', { }, 'root'),
    #above is link, the view, arguments passed to the view, and an alias for other things to reference it ( like {% url root %})
)

urlpatterns += patterns('', (
        r'^static/$',
        'django.views.static.serve',
        {'document_root': 'static' }
))

urlpatterns += patterns('oursite.views.city', 
	(r'^city/$', 'index', { 'city': True }, 'city_home_page'), 	#the home page of the city stuff, arguments are all the sub things
	
  (r'^city/city-page/$', 'index', { 'city': True }, 'city_page'),	#this says that when city_page is called, it will go to the city index view 
  (r'^city/city-qualities/(?P<action>\w+)/$', 'city', { }, 'city'), #<action> passes it in as an argument

  (r'^city/add-cities/(?P<action>\w+)/$', 'add_cities', { }, 'add_cities'),
  (r'^city/city-statistics/$', 'city_statistics', { }, 'city_statistics'),
  (r'^city/display-cities/$', 'display_cities', { }, 'display_cities'),
)

urlpatterns += patterns('oursite.views.home',
  (r'^home/$', 'index', { }, 'website_home_page'),
)
