from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.conf import settings
#more useful stuff for later

def index(request,
          city=False,
          ):

        context = {
                   'city': city,
                   'template_debug': settings.TEMPLATE_DEBUG,
                   }

        return render(request, 'city/index.html', context)