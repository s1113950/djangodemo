from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone
from django.utils.timezone import *
from django.utils import simplejson as json
from django.contrib.auth import authenticate, login
import sys
import os
import subprocess
import traceback
from datetime import datetime, time

from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.formsets import formset_factory
from django.contrib.auth.models import User
from django.core import serializers
from django.conf import settings

#useful stuff that in order to not forget i put here

from oursite.usefulPython.stringManip import find_nth	#putting useful python code in usefulPython folder

def city_statistics(request, action='default'):
	
	cityName = request.POST.get('stats').encode('ascii')
	
	context = {'cityName': cityName,
						}	#arguments to the template

	return render(request, 'city/city-statistics.html', context)