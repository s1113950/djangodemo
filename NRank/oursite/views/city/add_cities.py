from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone
from django.utils.timezone import *
from django.utils import simplejson as json
from django.contrib.auth import authenticate, login
import sys
import os
import subprocess
import traceback
from datetime import datetime, time

from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.forms.formsets import formset_factory
from django.contrib.auth.models import User
from django.core import serializers
from django.conf import settings

from oursite.forms.add_city_data_form import AddCityDataForm

from oursite.models.city.city import City
from oursite.models.city.city_qualities import CityQualities

#useful stuff that in order to not forget i put here

from oursite.usefulPython.stringManip import find_nth	#putting useful python code in usefulPython folder

def add_cities(request, action='default'):

	if action == 'display':
		cityName = request.POST.get('city_add').encode('ascii')
		
		form = AddCityDataForm()

		context = {'cityName': cityName,
							 'form': form,
			  			}	#arguments to the template


		return render(request, 'city/add-cities.html', context)

	elif action == 'add':
		form = AddCityDataForm(request.POST)

		if form.is_valid():
			region = form.cleaned_data['region']
			population = form.cleaned_data['population']
			geography = form.cleaned_data['geography']
			good_schools = forms.cleaned_data['good_schools']
			median_income = forms.cleaned_data['median_income']
			best_restaurant = forms.cleaned_data['best_restaurant']

			#city = City.objects.create(city_name=)
		
		else:
			print form.errors	#this is for checking in the terminal what went wrong
			messages.error(request, 'Error!', extra_tags='safe')
			return HttpResponse(1)
			#return HttpResponse(form.errors)	#throw error in template
			#eventually do error handling here

		form = AddCityDataForm()	#reinitialize to empty form for return back

		context = {'form': form,
							}

		return render(request, 'home/index.html')	#just return back to the main page to get rid of the form stuff