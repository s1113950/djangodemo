from oursite.views.city.index import index
from oursite.views.city.city import city
from oursite.views.city.add_cities import add_cities
from oursite.views.city.city_statistics import city_statistics
from oursite.views.city.display_cities import display_cities
#there are a lot of __init__.py and index.py, but it will all make sense later