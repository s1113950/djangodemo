from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.conf import settings
#more useful stuff for later

from oursite.forms.city_display_form import CityDisplayForm

def index(request):	#nothing fancy, just displays the first page
	
	form = CityDisplayForm()	#initialize an empty form

	context = {'form': form,
			  }

	return render(request, 'home/index.html', context)	#context is stuff passed to the template html, stuff in weird symbols basically