def find_nth(haystack, needle, n):	#python find() function, but better. Finds nth 'y' in hipyyy for example
  start = haystack.find(needle)
  while start >= 0 and n > 1:
      start = haystack.find(needle, start+len(needle))
      n -= 1
  return start