from django.contrib import admin

from oursite.models.city import CityQualities

admin.site.register(CityQualities)
#this is so the admin site, /admin/, can see the city table