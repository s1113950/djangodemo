from django.db import models

from oursite.models.city.city_qualities import CityQualities

class City(models.Model):
    db_table = "city"

    city_name = models.CharField(max_length=128)
    region = models.CharField(max_length=128)

    qualities = models.ForeignKey(CityQualities, related_name="city_qualities")

    def __unicode__(self):	#how each object will be defaultly viewed, in admin page and in a drop down for example
        return "City Name={0}".format(self.city_name)

    class Meta:
        app_label = 'oursite'