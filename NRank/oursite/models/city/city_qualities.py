from django.db import models

class CityQualities(models.Model):
	db_table = "city_qualities"

	city_name = models.CharField(max_length=128)
	population = models.IntegerField(blank=True, null=True)
	geography = models.CharField(max_length=128, blank=True, null=True)
	good_schools = models.NullBooleanField(blank=True, null=True)
	median_income = models.IntegerField(blank=True, null=True)
	best_restaurant = models.CharField(max_length=128, blank=True, null=True)

	def __unicode__(self):	#how each object will be defaultly viewed, in admin page and in a drop down for example
	    return "City Name={0}".format(self.city_name)

	class Meta:
	    app_label = 'oursite'