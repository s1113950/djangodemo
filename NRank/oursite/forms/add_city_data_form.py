from django import forms

class AddCityDataForm(forms.Form):
	region = forms.CharField(max_length=128)
	population = forms.IntegerField(required=False)
	geography = forms.CharField(max_length=128, required=False)
	good_schools = forms.BooleanField(required=False)
	median_income = forms.IntegerField(required=False)
	best_restaurant = forms.CharField(max_length=128, required=False)
	#more ideas to come later

	def clean(self):
		data = self.cleaned_data

		return data