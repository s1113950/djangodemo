from django import forms
from django.contrib.auth.models import User	#how to import users, useful so left here, remove later. Django handles users differently

class CityDisplayForm(forms.Form):
	city_add = forms.CharField(max_length=128, required=False)	#this will make it so that way can make one form (won't throw error if other fields blank)
	city_summary = forms.CharField(max_length=128, required=False)
	statistic = forms.CharField(max_length=128, required=False)

	#to override a drop down menu, for example, not needed right now
	""" 
	def __init__(self, *args, **kwargs):	
		choices = kwargs.pop('choices', None)
		super(CityDisplayForm, self).__init__(*args, **kwargs)
		if choices is not None:
			self.fields['employees'].queryset = choices
	"""

	def clean(self):	#when form.is_valid() is called, can put custom validation here
		data = self.cleaned_data

		return data
